<?php

use App\Http\Controllers\StreamerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WebhookController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [UserController::class, 'login'])->name('login');
Route::post('/webhook', [WebhookController::class, 'index'])->name('webhook');

Route::group([
    'middleware' => ['auth']
], static function (Router $router) {
    Route::post('/favourite-streamer', [StreamerController::class, 'set'])->name('streamer.set');
    Route::delete('/favourite-streamer', [StreamerController::class, 'delete'])->name('streamer.delete');
});
