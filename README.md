## Deployment

### Frontend

Frontend app can be deployed on firebase. They are perfect for static content apps - they handle everything from scaling to CDN which promises fast load speeds all around the globe.  

### Backend

##### App

I would definitely deploy this solution on AWS Elastic Beanstalk. Elastic Beanstalk automatically handles the deployment, from capacity provisioning, load balancing, auto-scaling to application health monitoring.

##### Database

The database can be an RDS cluster with at least two instances for high availability reasons. If we want low latency all around the globe, we can replicate the database in different availability zones.

## Bottlenecks and scalablility

At the moment I can identify one bottleneck in the backend application - twitch event processing. In order to remove this bottleneck, I'd suggest migrating the event processing to a microservice dealing only with this and deploy it on a separate beanstalk service. The other option is to go serverless and use Lambda for it. Doing this we eliminate any slow-downs of the main API and let AWS handle scaling for us.
