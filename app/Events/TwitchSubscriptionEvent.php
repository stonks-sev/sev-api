<?php

declare(strict_types=1);

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class TwitchSubscriptionEvent
 */
class TwitchSubscriptionEvent implements ShouldQueue, ShouldBroadcast
{
    use Dispatchable;
    use SerializesModels;
    use InteractsWithSockets;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $streamerId;

    /**
     * @var string
     */
    private $userName;
    
    /**
     * @var string
     */
    private $streamerName;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    public $message;

    /**
     * AccrueCommissionEvent constructor.
     *
     * @param int      $userId
     * @param int      $poolId
     * @param int      $level
     */
    public function __construct(int $userId, string $userName, int $streamerId, string $streamerName, string $type)
    {
        $this->userId           = $userId;
        $this->streamerId       = $streamerId;
        $this->userName         = $userName;
        $this->streamerName     = $streamerName;
        $this->type             = $type;

        $this->message = sprintf("%s has a new follower - %s", $streamerName, $userName);
    }

    public function broadcastOn()
    {
        return [strtolower($this->streamerName)];
    }

    public function broadcastAs()
    {
        return 'event';
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getStreamerId(): int
    {
        return $this->streamerId;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getStreamerName(): string
    {
        return $this->streamerName;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
