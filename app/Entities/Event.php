<?php

declare(strict_types=1);

namespace App\Entities;

use App\Traits\Entity\HasCreatedAtTimestampField;
use App\Traits\Entity\HasUpdatedAtTimestampField;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Event
 *
 * @ORM\Entity
 * @ORM\Table(name="events")
 */
class Event
{
    use HasCreatedAtTimestampField;
    use HasUpdatedAtTimestampField;

    /** @var int */
    const EVENT_TYPE_FOLLOW = 1;
    /** @var int */
    const EVENT_TYPE_SUBSCRIPTION = 2;
    /** @var array */
    const MAP_TWITCH_EVENT_TO_EVENT = [
        'channel.follow' => self::EVENT_TYPE_FOLLOW,
        'channel.subscription' => self::EVENT_TYPE_SUBSCRIPTION
    ];
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true}, nullable=false)
     */
    private $id;

    /**
     * @ORM\JoinColumn(name="streamer_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entities\Streamer", inversedBy="events")
     */
    private $streamer;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="viewer_name", type="string")
     */
    private $viewerName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \App\Entities\Streamer
     */
    public function getStreamer(): Streamer
    {
        return $this->streamer;
    }

    /**
     * @param \App\Entities\Streamer $streamer
     * 
     * @return Event
     */
    public function setStreamer(Streamer $streamer): self
    {
        $this->streamer = $streamer;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * 
     * @return Event
     */
    public function setType(int $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getViewerName(): string
    {
        return $this->viewerName;
    }

    /**
     * @param string $viewerName
     * 
     * @return Event
     */
    public function setViewerName(string $viewerName): self
    {
        $this->viewerName = $viewerName;
        return $this;
    }
}