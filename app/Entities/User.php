<?php

declare(strict_types=1);

namespace App\Entities;

use App\Traits\Entity\HasCreatedAtTimestampField;
use App\Traits\Entity\HasUpdatedAtTimestampField;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 *
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    use HasCreatedAtTimestampField;
    use HasUpdatedAtTimestampField;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true}, nullable=false)
     */
    private $id;

    /**
     * @var \App\Entities\Streamer|null
     *
     * @ORM\ManyToOne(targetEntity="\App\Entities\Streamer", inversedBy="users")
     * @ORM\JoinColumn(name="favourite_streamer_id", referencedColumnName="id", nullable=true)
     */
    private $favouriteStreamer;

    /**
     * @var int
     *
     * @ORM\Column(name="twitch_user_id", type="integer")
     */
    private $twtichUserId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return null|\App\Entities\Streamer
     */
    public function getFavouriteStreamer(): ?Streamer
    {
        return $this->favouriteStreamer;
    }

    /**
     * @param null|\App\Entities\Streamer $streamer
     * 
     * @return User
     */
    public function setFavouriteStreamer(?Streamer $streamer = null): self
    {
        $this->favouriteStreamer = $streamer;
        return $this;
    }

    /**
     * @return int
     */
    public function getTwitchUserId(): int
    {
        return $this->twtichUserId;
    }

    /**
     * @param int $userId
     * 
     * @return User
     */
    public function setTwitchUserId(int $userId): self
    {
        $this->twtichUserId = $userId;
        return $this;
    }
}