<?php

declare(strict_types=1);

namespace App\Entities;

use App\Traits\Entity\HasCreatedAtTimestampField;
use App\Traits\Entity\HasUpdatedAtTimestampField;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Streamer
 *
 * @ORM\Entity
 * @ORM\Table(name="streamers")
 */
class Streamer
{
    use HasCreatedAtTimestampField;
    use HasUpdatedAtTimestampField;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="bigint", options={"unsigned":true}, nullable=false)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="streamer_id", type="integer", nullable=true)
     */
    private $streamerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string")
     */
    private $language;

    /**
     * @var \Doctrine\ORM\PersistentCollection|\App\Entities\Event[]
     * @ORM\OneToMany(targetEntity="\App\Entities\Event", mappedBy="streamer")
     */
    private $events;

    /**
     * @var \Doctrine\ORM\PersistentCollection|\App\Entities\User[]
     * @ORM\OneToMany(targetEntity="\App\Entities\User", mappedBy="favouriteStreamer")
     */
    private $users;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return int
     */
    public function getStreamerId(): int
    {
        return $this->streamerId;
    }

    /**
     * @param int $id
     * 
     * @return Streamer
     */
    public function setStreamerId(int $id): self
    {
        $this->streamerId = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * 
     * @return Streamer
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * 
     * @return Streamer
     */
    public function setLanguage(string $language): self
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return null|PersistentCollection
     */
    public function getEvents(): ?PersistentCollection
    {
        return $this->events;
    }

    /**
     * @return null|PersistentCollection
     */
    public function getUsers(): ?PersistentCollection
    {
        return $this->users;
    }
}