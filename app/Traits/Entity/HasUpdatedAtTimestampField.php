<?php

declare(strict_types=1);

namespace App\Traits\Entity;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait HasUpdatedAtTimestampField
 *
 * @package pp\Traits\Entity
 */
trait HasUpdatedAtTimestampField
{
    /**
     * @var DateTime $updated
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * @return \Carbon\CarbonInterface
     */
    public function getUpdatedAt(): CarbonInterface
    {
        return Carbon::instance($this->updatedAt);
    }

    /**
     * @param \Carbon\CarbonInterface $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(CarbonInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt->toDateTime();

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateUpdatedAtTimestamp(): void
    {
        $this->setUpdatedAt(Carbon::now());
    }
}
