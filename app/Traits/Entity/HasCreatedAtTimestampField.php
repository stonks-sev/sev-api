<?php

declare(strict_types=1);

namespace App\Traits\Entity;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait HasCreatedAtTimestampField
 *
 * @package App\Traits\Entity
 */
trait HasCreatedAtTimestampField
{
    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @return \Carbon\CarbonInterface
     */
    public function getCreatedAt(): CarbonInterface
    {
        return Carbon::instance($this->createdAt);
    }

    /**
     * @param \Carbon\CarbonInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(CarbonInterface $createdAt): self
    {
        $this->createdAt = $createdAt->toDateTime();

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateCreatedAtTimestamp(): void
    {
        if (null === $this->createdAt) {
            $this->setCreatedAt(Carbon::now());
        }
    }
}
