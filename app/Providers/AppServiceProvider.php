<?php

namespace App\Providers;

use App\Contracts\Repository\EventRepositoryInterface;
use App\Contracts\Repository\StreamerRepositoryInterface;
use App\Contracts\Repository\UserRepositoryInterface;
use App\Contracts\Service\EventServiceInterface;
use App\Contracts\Service\StreamerServiceInterface;
use App\Contracts\Service\UserServiceInterface;
use App\Repositories\EventRepository;
use App\Repositories\StreamerRepository;
use App\Repositories\UserRepository;
use App\Services\EventService;
use App\Services\StreamerService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @return  array
     */
    public function provides(): array
    {
        return [
            EventServiceInterface::class,
            StreamerServiceInterface::class,
            UserServiceInterface::class,
            EventRepositoryInterface::class,
            StreamerRepositoryInterface::class,
            UserRepositoryInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();
        $this->registerRepositories();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * @return  void
     */
    private function registerServices(): void
    {
        $this->app->singleton(EventServiceInterface::class, EventService::class);
        $this->app->singleton(StreamerServiceInterface::class, StreamerService::class);
        $this->app->singleton(UserServiceInterface::class, UserService::class);
    }

    /**
     * @return  void
     */
    private function registerRepositories(): void
    {
        $this->app->singleton(EventRepositoryInterface::class, EventRepository::class);
        $this->app->singleton(StreamerRepositoryInterface::class, StreamerRepository::class);
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
    }
}
