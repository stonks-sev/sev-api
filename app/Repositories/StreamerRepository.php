<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\Repository\StreamerRepositoryInterface;
use App\Entities\Streamer;
use App\Repositories\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class StreamerRepository
 */
class StreamerRepository extends ServiceEntityRepository implements StreamerRepositoryInterface
{
    /**
     * StreamerRepository constructor.
     *
     * @param  \Doctrine\Common\Persistence\ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Streamer::class);
    }

    /**
     * @param  int $id
     *
     * @return  Streamer
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findOrFail(int $id): Streamer
    {
        return $this->createQueryBuilder('s')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param  int $streamerId
     *
     * @return  Streamer
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findByStreamerIdOrFail(int $streamerId): Streamer
    {
        return $this->createQueryBuilder('s')
            ->where('s.streamerId = :id')
            ->setParameter('id', $streamerId)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param  int $streamerId
     *
     * @return  null|Streamer
     */
    public function findByStreamerId(int $streamerId): ?Streamer
    {
        return $this->createQueryBuilder('s')
            ->where('s.streamerId = :id')
            ->setParameter('id', $streamerId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}