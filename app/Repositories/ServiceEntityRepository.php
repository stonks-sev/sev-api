<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use LogicException;

/**
 * Optional EntityRepository base class with a simplified constructor (for autowiring).
 *
 * To use in your class, inject the "registry" service and call
 * the parent constructor. For example:
 *
 * class YourEntityRepository extends ServiceEntityRepository
 * {
 *     public function __construct(RegistryInterface $registry)
 *     {
 *         parent::__construct($registry, YourEntity::class);
 *     }
 * }
 */
class ServiceEntityRepository extends EntityRepository implements ServiceEntityRepositoryInterface
{
    /**
     * @param \Doctrine\Common\Persistence\ManagerRegistry $registry
     * @param string                                       $entityClass The class name of the entity this repository
     *                                                                  manages
     */
    public function __construct(ManagerRegistry $registry, $entityClass)
    {
        /** @var \Doctrine\ORM\EntityManager|null $manager */
        $manager = $registry->getManagerForClass($entityClass);

        if ($manager === null) {
            $message = 'Could not find the entity manager for class "%s". ';
            $message .= 'Check your Doctrine configuration to make sure ';
            $message .= 'it is configured to load this entity’s metadata.';
            throw new LogicException(sprintf($message, $entityClass));
        }

        parent::__construct($manager, $manager->getClassMetadata($entityClass));
    }
}
