<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\Repository\EventRepositoryInterface;
use App\Entities\Event;
use App\Repositories\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class EventRepository
 */
class EventRepository extends ServiceEntityRepository implements EventRepositoryInterface
{
    /**
     * EventRepository constructor.
     *
     * @param  \Doctrine\Common\Persistence\ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @param  int $id
     *
     * @return  Event
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findOrFail(int $id): Event
    {
        return $this->createQueryBuilder('m')
            ->where('m.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }
}