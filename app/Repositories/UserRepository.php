<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Contracts\Repository\UserRepositoryInterface;
use App\Entities\Streamer;
use App\Entities\User;
use App\Repositories\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param  \Doctrine\Common\Persistence\ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param  int $id
     *
     * @return  User
     * 
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findOrFail(int $id): User
    {
        return $this->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param  int $twitchUserId
     *
     * @return  User
     * 
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findByTwitchUserIdOrFail(int $twitchUserId): User
    {
        return $this->createQueryBuilder('u')
            ->where('u.twtichUserId = :id')
            ->setParameter('id', $twitchUserId)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param  int $twitchUserId
     *
     * @return  null|User
     */
    public function findByTwitchUserId(int $twitchUserId): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.twtichUserId = :id')
            ->setParameter('id', $twitchUserId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}