<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\AbstractPaginator;
use Symfony\Component\HttpFoundation;
use Vci\Api\v1\Http\Resources\NativeResource;
use Vci\Api\v1\Http\Resources\NativeResourceCollection;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * Class Response
 *
 * @package Vci\Http\Responses\Api
 */
class Response extends IlluminateResponse implements Responsable
{
    /**
     * @var int
     */
    private $status;

    /**
     * @var int
     */
    private $options;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var mixed
     */
    private $pagination;

    /**
     * @var array
     */
    private $messages = [];

    /**
     * @var mixed
     */
    private $validation;

    /**
     * @var mixed
     */
    private $backtrace;

    /**
     * @var mixed
     */
    private $eventId;

    /**
     * @var float|null
     */
    private $duration;

    /**
     * @var float|null
     */
    private $memory;

    /**
     * @var float|null
     */
    private $memoryPeak;

    /**
     * JsonResponse constructor.
     *
     * @param mixed  $data
     * @param int    $status
     * @param array  $headers
     * @param int    $options
     * @param string $version
     */
    protected function __construct(
        $data = null,
        $status = 200,
        array $headers = [],
        $options = 0,
        string $version = null
    ) {
        $this->status  = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->data    = $data;
        $this->version = $version;
    }

    /**
     * @param mixed $data
     * @param int   $status
     * @param array $headers
     * @param int   $options
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public static function make($data = null, $status = 200, array $headers = [], $options = 0): Response
    {
        $version = app()->getVersion();
        return new static($data, $status, $headers, $options, $version);
    }

    /**
     * @param string $text
     * @param mixed  $code
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function addErrorMessage(string $text, $code = 0): Response
    {
        return $this->addMessage('error', $text, $code);
    }

    /**
     * @param string $text
     *
     * @return bool
     */
    public function hasErrorMessage(string $text): bool
    {
        foreach ($this->messages as $message) {
            if ($message['severity'] === 'error' && $message['text'] === $text) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $severity
     * @param string $text
     * @param mixed  $code
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function addMessage(string $severity, string $text, $code = 0): Response
    {
        $message = [
            'severity' => $severity,
            'text'     => $text,
            'code'     => $code,
        ];

        $this->messages[] = $message;

        return $this;
    }

    /**
     * @param string $text
     * @param mixed  $code
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function addSuccessMessage(string $text, $code = 0): Response
    {
        return $this->addMessage('success', $text, $code);
    }

    /**
     * @param int  $status
     * @param null $text
     * @return $this|object
     */
    public function setStatusCode(int $status, $text = NULL): object
    {
        $this->status = $status;

        return $this;
    }


    /**
     * @param array $fields
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setValidation(array $fields): Response
    {
        $this->validation = $fields;

        return $this;
    }

    /**
     * @param array $backtrace
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setBacktrace(array $backtrace): Response
    {
        $this->backtrace = $backtrace;

        return $this;
    }

    /**
     * @param string $eventId
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setEventId($eventId): Response
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @param float $duration
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setDuration(float $duration): Response
    {
        $this->duration = round($duration, 4);

        return $this;
    }

    /**
     * @param float $memory
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setMemory(float $memory): Response
    {
        $this->memory = round($memory, 4);

        return $this;
    }

    /**
     * @param float $memoryPeak
     *
     * @return \Vci\Api\v1\Http\Responses\Response
     */
    public function setMemoryPeak(float $memoryPeak): Response
    {
        $this->memoryPeak = round($memoryPeak, 4);

        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request): JsonResponse
    {
        $additional = [];
        $data       = [];

        $this->normalizeData($request);

        if (
            $this->status === HttpFoundation\Response::HTTP_OK ||
            $this->status === HttpFoundation\Response::HTTP_CREATED
        ) {
            if (!empty($this->pagination)) {
                $data['result']     = $this->data;
                $data['pagination'] = $this->pagination;
            } else {
                $data['result'] = $this->data;
            }
        }

        $data['version'] = $this->version;

        if (defined('LARAVEL_START')) {
            $this->setDuration(microtime(true) - LARAVEL_START);
        }

        $this->setMemory(memory_get_usage() / 1024 / 1024);
        $this->setMemoryPeak(memory_get_peak_usage() / 1024 / 1024);

        if (!empty($this->messages)) {
            $data['messages'] = $this->messages;
        }

        if (!empty($this->validation)) {
            $data['validation'] = $this->validation;
        }

        if (!empty($this->backtrace)) {
            $data['backtrace'] = $this->backtrace;
        }

        if (!empty($this->backtrace)) {
            $data['eventId'] = $this->eventId;
        }

        if ($this->duration !== null) {
            $data['duration'] = $this->duration;
        }

        if ($this->memory !== null) {
            $data['memory-c'] = $this->memory;
        }

        if ($this->memoryPeak !== null) {
            $data['memory-p'] = $this->memoryPeak;
        }

        $data = array_merge($data, $additional);

        $response = new JsonResponse($data, $this->status, $this->headers, $this->options);

        return $response;
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    private function normalizeData(Request $request): void
    {
        if (is_bool($this->data)) {
            $this->data = intval($this->data);
        } elseif ($this->data instanceof ResourceCollection) {
            if ($this->data->resource instanceof AbstractPaginator) {
                $this->pagination = (new PaginatedResourceResponse($this->data))->getPagination($request);
            }
        } elseif ($this->data instanceof NativeResource || $this->data instanceof NativeResourceCollection) {
            $this->data = $this->data->toArray();
        } elseif ($this->data instanceof JsonResource) {
            $this->data = array_replace(
                $this->data->resolve($request),
                $this->data->with($request),
                $this->data->additional
            );
        }
    }
}
