<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Entities\Streamer;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FavStreamerResource
 */
class FavStreamerResource extends JsonResource
{
    /**
     * @var \App\Entities\Streamer
     */
    private $streamer;

    /**
     * FavStreamerResource constructor.
     *
     * @param \App\Entities\Streamer $user
     */
    public function __construct(Streamer $streamer)
    {
        $this->streamer = $streamer;
        parent::__construct($streamer);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->streamer->getId(),
            'name'          => $this->streamer->getName(),
            'laguage'       => $this->streamer->getLanguage(),
            'streamer_id'   => $this->streamer->getStreamerId(),
        ];
    }
}
