<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Entities\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 */
class UserResource extends JsonResource
{
    /**
     * @var \App\Entities\User
     */
    private $user;

    /**
     * UserResource constructor.
     *
     * @param \App\Entities\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct($user);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'             => $this->user->getId(),
            'twitch_user_id' => $this->user->getTwitchUserId(),
            'fav_streamer'   => $this->user->getFavouriteStreamer() ? new FavStreamerResource($this->user->getFavouriteStreamer()) : null,
        ];
    }
}
