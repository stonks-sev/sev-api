<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Contracts\Http\Requests\SetFavStreamerRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SetFavStreamerRequest
 */
class SetFavStreamerRequest extends FormRequest implements SetFavStreamerRequestInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->input('name');
    }

    /**
     * @return int
     */
    public function getStreamerId(): int
    {
        return (int)$this->input('streamer_id');
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'string',
                'required',
                'min:2'
            ],
            'streamer_id' => [
                'integer',
                'required'
            ],
        ];
    }
}
