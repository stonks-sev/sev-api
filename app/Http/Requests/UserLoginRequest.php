<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Contracts\Http\Requests\UserLoginRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserLoginRequest
 */
class UserLoginRequest extends FormRequest implements UserLoginRequestInterface
{
    /**
     * @return string
     */
    public function getToken(): string
    {
        return (string)$this->input('token');
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'token' => [
                'string',
                'required'
            ]
        ];
    }
}
