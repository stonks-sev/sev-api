<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Contracts\Repository\UserRepositoryInterface;
use App\Services\TwitchService;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class Auth
 */
class Auth
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws HttpException
     */
    public function handle(Request $request, Closure $next)
    {
        $twitchService = app()->make(TwitchService::class);
        $userRepository = app()->make(UserRepositoryInterface::class);

        $header = $request->header('Authorization');

        if (!$header) {
            throw new HttpException(
                Response::HTTP_UNAUTHORIZED,
                'Unauthenticated.'
            );
        }

        $auth = $twitchService->isAuthenticated($header);
        if (!$auth) {
            throw new HttpException(
                Response::HTTP_UNAUTHORIZED,
                'Unauthenticated.'
            );
        }

        try {
            // Get user info
            $user = $userRepository->findByTwitchUserIdOrFail((int)$auth['user_id']);

            $request->merge(["user" => $user]);
        } catch (Exception $e) {
            throw new HttpException(
                Response::HTTP_UNAUTHORIZED,
                'Unauthenticated.'
            );
        }

        return $next($request);
    }
}
