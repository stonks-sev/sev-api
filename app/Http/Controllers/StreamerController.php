<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contracts\Repository\StreamerRepositoryInterface;
use App\Contracts\Service\StreamerServiceInterface;
use App\Contracts\Service\UserServiceInterface;
use App\Http\Requests\SetFavStreamerRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StreamerController extends Controller
{
    /**
     * @var App\Contracts\Service\StreamerServiceInterface
     */
    private StreamerServiceInterface $streamerService;

    /**
     * @var App\Contracts\Service\UserServiceInterface
     */
    private UserServiceInterface $userService;

    /**
     * @var App\Contracts\Repository\StreamerRepositoryInterface
     */
    private StreamerRepositoryInterface $streamerRepository;

    public function __construct(
        StreamerServiceInterface $streamerService,
        UserServiceInterface $userService,
        StreamerRepositoryInterface $streamerRepository
    ) {
        $this->streamerService      = $streamerService;
        $this->userService          = $userService;
        $this->streamerRepository   = $streamerRepository;
    }

    public function set(SetFavStreamerRequest $request): Response
    {
        $user = $request->all()["user"];

        // check to see if streamer already exists in database
        $streamer = $this->streamerRepository->findByStreamerId($request->getStreamerId());
        if (!$streamer)  {
            // if not create streamer
            $streamer = $this->streamerService->create($request);
        }
        
        // then assign to user
        $user = $this->userService->setFavouriteStreamer($user, $streamer);

        return new Response(new UserResource($user), Response::HTTP_OK);
    }

    public function delete(Request $request): Response
    {
        $user = $request->all()["user"];
        $this->userService->removeFavouriteStreamer($user);

        return new Response('ok.', Response::HTTP_OK);
    }
}