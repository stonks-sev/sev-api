<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contracts\Repository\UserRepositoryInterface;
use App\Contracts\Service\UserServiceInterface;
use App\Http\Requests\UserLoginRequest;
use App\Http\Resources\UserResource;
use App\Services\TwitchService;
use App\Services\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @var App\Contracts\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var App\Contracts\Service\UserServiceInterface
     */
    private UserServiceInterface $userService;

    /**
     * @var App\Contracts\Repository\UserRepositoryInterface
     */
    private TwitchService $twitchService;

    /**
     * @param UserRepositoryInterface   $userRepository
     * @param UserServiceInterface      $userService
     * @param TwitchService             $twitchService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserServiceInterface $userService,
        TwitchService $twitchService
    ) {
        $this->userRepository = $userRepository;
        $this->userService    = $userService;
        $this->twitchService  = $twitchService;
    }

    // Register a new user
    public function login(UserLoginRequest $request): Response
    {
        $twitchUser = $this->twitchService->isAuthenticated($request->getToken());
        if (!$twitchUser) {
            return new Response('Unauthenticated.', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->userRepository->findByTwitchUserId((int)$twitchUser['user_id']);
        if (!$user) {
            $user = $this->userService->create($request, (int)$twitchUser['user_id']);
        }

        $userResource = new UserResource($user);
        return new Response($userResource, Response::HTTP_OK);
    }
}