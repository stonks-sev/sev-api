<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\TwitchSubscriptionEvent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    
    public function __construct()
    {
    }

    // handle twitch events
    public function index(Request $request): Response
    {
        Log::info('Incoming request in WbhookController:index');

        if (!$this->verifyRequest($request)) {
            Log::error('Incoming request in WbhookController:index not verified');
            return new Response('', Response::HTTP_BAD_REQUEST);
        }

        $data = json_decode($request->getContent(), true);

        if ($data['subscription']['status'] == 'webhook_callback_verification_pending') {
            return $this->verifySubscription($data);
        }

        Log::info('Firing TwitchSubscriptionEvent event');
        event(new TwitchSubscriptionEvent((int)$data['event']['user_id'], $data['event']['user_name'], (int)$data['event']['broadcaster_user_id'], $data['event']['broadcaster_user_name'], $data['subscription']['type']));

        return new Response('ok.', Response::HTTP_OK);
    }

    /**
     * @param array $data
     * 
     * @return Response
     */
    private function verifySubscription(array $data): Response
    {
        Log::info('Verifying subscription in WbhookController:verifySubscription / ' . $data["challenge"]);
        $response = new Response($data["challenge"], Response::HTTP_OK);
        $response->header('Content-Type', 'text/plain');

        return $response;
    }

    /**
     * @param Request $request
     * 
     * @return bool
     */
    private function verifyRequest(Request $request): bool
    {
        $message = $request->header('Twitch-Eventsub-Message-Id') . $request->header('Twitch-Eventsub-Message-Timestamp') . $request->getContent();
        $signature = hash_hmac('sha256', $message, env('TWITCH_CALLBACK_SECRET'));
        $expectedSignature = 'sha256=' . $signature;

        return $request->header('Twitch-Eventsub-Message-Signature') == $expectedSignature;
    }
}