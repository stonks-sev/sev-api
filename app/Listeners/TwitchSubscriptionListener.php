<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Contracts\Service\EventServiceInterface;
use App\Entities\Event;
use App\Events\TwitchSubscriptionEvent;
use Illuminate\Support\Facades\Log;

/**
 * Class TwitchSubscriptionListener
 */
class TwitchSubscriptionListener
{
    /**
     * @param \App\Events\TwitchSubscriptionEvent                   $event
     */
    public function handle(TwitchSubscriptionEvent $event): void {
        Log::info('TwitchSubscriptionListener reached');
        $eventService = app()->make(EventServiceInterface::class);

        $streamerId = $event->getStreamerId();
        $type       = Event::MAP_TWITCH_EVENT_TO_EVENT[$event->getType()];
        $viewerName = $event->getUserName();

        $eventService->create($streamerId, $type, $viewerName);
    }
}
