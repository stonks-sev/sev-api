<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\Repository\EventRepositoryInterface;
use App\Contracts\Repository\StreamerRepositoryInterface;
use App\Contracts\Service\EventServiceInterface;
use App\Entities\Event;
use App\Entities\Streamer;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class EventService
 */
class EventService implements EventServiceInterface
{
    /**
     * @var  EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
    * @var  \App\Contracts\Repository\EventRepositoryInterface
    */
    protected EventRepositoryInterface $eventRepository;

    /**
    * @var  \App\Contracts\Repository\StreamerRepositoryInterface
    */
    protected StreamerRepositoryInterface $streamerRepository;

    /**
     * EventService constructor.
     *
     * @param  \Doctrine\ORM\EntityManagerInterface                     $entityManager
     * @param  \App\Contracts\Repository\EventRepositoryInterface       $eventRepository
     * @param  \App\Contracts\Repository\StreamerRepositoryInterface    $streamerRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        EventRepositoryInterface $eventRepository,
        StreamerRepositoryInterface $streamerRepository
    ) {
        $this->entityManager        = $entityManager;
        $this->eventRepository      = $eventRepository;
        $this->streamerRepository   = $streamerRepository;
    }

    /**
     * @param int $streamerId
     * @param int $type
     * @param string $viewerName
     *
     * @return  \App\Entities\Event
     * 
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function create(int $streamerId, int $type, string $viewerName): Event
    {
        $streamer = $this->streamerRepository->findByStreamerIdOrFail($streamerId);

        $event = new Event();
        $event->setStreamer($streamer)
            ->setType($type)
            ->setViewerName($viewerName)
            ->setCreatedAt(Carbon::now());

        $this->entityManager->persist($event);
        $this->entityManager->flush();

        return $event;
    }
}
