<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\Http\Requests\UserLoginRequestInterface;
use App\Contracts\Repository\UserRepositoryInterface;
use App\Contracts\Service\UserServiceInterface;
use App\Entities\Streamer;
use App\Entities\User;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserService
 */
class UserService implements UserServiceInterface
{
    /**
     * @var  EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
    * @var  \App\Contracts\Repository\UserRepositoryInterface
    */
    protected UserRepositoryInterface $userRepository;

    /**
     * EventService constructor.
     *
     * @param  \Doctrine\ORM\EntityManagerInterface                 $entityManager
     * @param  \App\Contracts\Repository\UserRepositoryInterface    $userRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepositoryInterface $userRepository
    ) {
        $this->entityManager    = $entityManager;
        $this->userRepository   = $userRepository;
    }

    /**
     * @param App\Contracts\Http\Requests\UserLoginRequestInterface $request
     * @param int                                                   $twitchUserId
     *
     * @return \App\Entities\User
     */
    public function create(UserLoginRequestInterface $request, int $twitchUserId): User
    {
        $user = new User();
        $user->setTwitchUserId($twitchUserId)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param  \App\Entities\User     $user
     * @param  \App\Entities\Streamer $streamer
     *
     * @return  \App\Entities\User
     */
    public function setFavouriteStreamer(User $user, Streamer $streamer): User
    {
        $user->setFavouriteStreamer($streamer);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param  \App\Entities\User $user
     *
     * @return \App\Entities\User
     */
    public function removeFavouriteStreamer(User $user): User
    {
        $user->setFavouriteStreamer();

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
