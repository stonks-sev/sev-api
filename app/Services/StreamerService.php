<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\Http\Requests\SetFavStreamerRequestInterface;
use App\Contracts\Repository\StreamerRepositoryInterface;
use App\Contracts\Service\StreamerServiceInterface;
use App\Entities\Streamer;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\Facades\Log;

/**
 * Class StreamerService
 */
class StreamerService implements StreamerServiceInterface
{
    /**
     * @var  EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
    * @var  \App\Contracts\Repository\StreamerRepositoryInterface
    */
    protected StreamerRepositoryInterface $streamerRepository;

    /**
    * @var  \App\Services\TwitchService
    */
    protected TwitchService $twitchService;

    /**
     * StreamerService constructor.
     *
     * @param  \Doctrine\ORM\EntityManagerInterface                     $entityManager
     * @param  \App\Contracts\Repository\StreamerRepositoryInterface    $streamerRepository
     * @param  \AApp\Services\TwitchService                             $twitchService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        StreamerRepositoryInterface $streamerRepository,
        TwitchService $twitchService
    ) {
        $this->entityManager        = $entityManager;
        $this->streamerRepository   = $streamerRepository;
        $this->twitchService        = $twitchService;
    }

    /**
     * @param App\Contracts\Http\Requests\SetFavStreamerRequestInterface $request
     *
     * @return \App\Entities\Streamer
     */
    public function create(SetFavStreamerRequestInterface $request): Streamer
    {
        Log::info('Creating streamer ' . $request->getName());

        $streamer = new Streamer();
        $streamer->setName($request->getName())
            ->setStreamerId($request->getStreamerId())
            ->setCreatedAt(Carbon::now())
            ->setCreatedAt(Carbon::now());

        $this->entityManager->persist($streamer);
        $this->entityManager->flush();

        $this->twitchService->subscribeToChannel($streamer);

        return $streamer;
    }

    /**
     * @param  int $streamerId
     *
     * @return  \App\Entities\Streamer
     *
     * @throws    \Doctrine\ORM\NoResultException
     * @throws    \Doctrine\ORM\NonUniqueResultException
     */
    public function show(int $streamerId): Streamer
    {
        return new Streamer();
    }

    /**
     * @param  int $streamerId
     *
     * @return  bool
     *
     * @throws    \Doctrine\ORM\NoResultException
     * @throws    \Doctrine\ORM\NonUniqueResultException
     */
    public function delete(int $streamerId): bool
    {
        return true;
    }
}
