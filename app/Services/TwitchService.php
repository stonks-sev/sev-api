<?php

declare(strict_types=1);

namespace App\Services;

use App\Entities\Streamer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Class TwitchService
 */
class TwitchService
{
    /**
     * MatrixService constructor.
     *
     * @param  \Doctrine\ORM\EntityManagerInterface    $entityManager
     * @param  \Vci\Modules\Matrix\Repository\MatrixRepositoryInterface $matrixRepository
     */
    public function __construct()
    {
    }

    /**
     * @param string $token
     * 
     * @return null|array
     */
    public function isAuthenticated(string $token): ?array
    {
        try {
            $httpClient  = new Client();
            $endpointUrl = 'https://id.twitch.tv/oauth2/validate';

            $response = $httpClient->get($endpointUrl, [
                'headers'        => ['Authorization' => sprintf("OAuth %s", $token)]
            ]);
            $result   = $response->getBody()->getContents();

            if ($response->getStatusCode() !== Response::HTTP_OK || empty($result)) {
                return null;
            }

            try {
                $result = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $ex) {
                return null;
            }

            return $result;
        } catch (RequestException $exception) {
            return null;
        }
    }

    public function getAppToken(): ?string
    {
        try {
            $httpClient  = new Client();
            $endpointUrl = sprintf("https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials",
                env("TWITCH_CLIENT_ID"),
                env("TWITCH_CLIENT_SECRET")
            );

            $response = $httpClient->post($endpointUrl);
            $result   = $response->getBody()->getContents();

            if ($response->getStatusCode() !== Response::HTTP_OK || empty($result)) {
                return null;
            }

            try {
                $result = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $ex) {
                return null;
            }

            return $result["access_token"];
        } catch (RequestException $exception) {
            return null;
        }
    }

    public function subscribeToChannel(Streamer $streamer): bool
    {
        Log::info('Subscribing to channel ' . $streamer->getName());

        $token = $this->getAppToken();

        $this->subscribeForFollow($streamer, $token);
        $this->subscribeForSubscription($streamer, $token);
        return true;
    }

    private function subscribeForFollow(Streamer $streamer, string $token): bool
    {
        try {
            $httpClient  = new Client();
            $endpointUrl = 'https://api.twitch.tv/helix/eventsub/subscriptions';

            $data = [
                "type" => "channel.follow",
                "version" => "1",
                "condition" => [
                    "broadcaster_user_id" => (string)$streamer->getStreamerId()
                ],
                "transport" => [
                    "method" => "webhook",
                    "callback" => env('TWITCH_CALLBACK_URL'),
                    "secret" => env('TWITCH_CALLBACK_SECRET')
                ]
            ];

            Log::info(json_encode($data));

            $response = $httpClient->post($endpointUrl, [
                'headers'        => [
                    'Authorization' => sprintf("Bearer %s", $token),
                    'Client-ID' => env('TWITCH_CLIENT_ID'),
                    'Content-Type' => 'application/json'
                ],
                RequestOptions::JSON => $data,
            ]);
            $result   = $response->getBody()->getContents();

            if ($response->getStatusCode() !== Response::HTTP_ACCEPTED || empty($result)) {
                Log::error('Response in subscribeForFollow is not OK.');
                Log::error($response->getStatusCode());
                Log::error($result);
                return false;
            }

            try {
                $result = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $ex) {
                Log::error('Could not decode the result of subscribeForFollow');
                return false;
            }

            return true;
        } catch (RequestException $exception) {
            Log::error('RequestException in subscribeForFollow: ' . $exception->getMessage());
            return false;
        }
    }

    private function subscribeForSubscription(Streamer $streamer, string $token)
    {
        try {
            $httpClient  = new Client();
            $endpointUrl = 'https://api.twitch.tv/helix/eventsub/subscriptions';

            $data = [
                "type" => "channel.subscribe",
                "version" => "1",
                "condition" => [
                    "broadcaster_user_id" => (string)$streamer->getStreamerId()
                ],
                "transport" => [
                    "method" => "webhook",
                    "callback" => env('TWITCH_CALLBACK_URL'),
                    "secret" => env('TWITCH_CALLBACK_SECRET')
                ]
            ];

            $response = $httpClient->post($endpointUrl, [
                'headers'        => [
                    'Authorization' => sprintf("Bearer %s", $token),
                    'Client-ID' => env('TWITCH_CLIENT_ID'),
                    'Content-Type' => 'application/json'
                ],
                RequestOptions::JSON => $data,
            ]);
            $result   = $response->getBody()->getContents();

            if ($response->getStatusCode() !== Response::HTTP_ACCEPTED || empty($result)) {
                Log::error('Response in subscribeForSubscription is not OK.');
                Log::error($response->getStatusCode());
                Log::error($result);
                return false;
            }

            try {
                $result = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $ex) {
                Log::error('Could not decode the result of subscribeForSubscription');
                return false;
            }

            return true;
        } catch (RequestException $exception) {
            Log::error('RequestException in subscribeForSubscription: ' . $exception->getMessage());
            return false;
        }
    }
}
