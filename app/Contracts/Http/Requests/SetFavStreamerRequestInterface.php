<?php

declare(strict_types=1);

namespace App\Contracts\Http\Requests;

/**
 * Interface SetFavStreamerRequestInterface
 */
interface SetFavStreamerRequestInterface
{
    /**
    * @return string
    */
    public function getName(): string;
    /**
    * @return int
    */
    public function getStreamerId(): int;
}
