<?php

declare(strict_types=1);

namespace App\Contracts\Http\Requests;

/**
 * Interface UserLoginRequestInterface
 */
interface UserLoginRequestInterface
{
    /**
    * @return string
    */
    public function getToken(): string;
}
