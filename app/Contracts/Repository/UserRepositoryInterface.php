<?php

declare(strict_types=1);

namespace App\Contracts\Repository;

use App\Entities\User;
use Doctrine\Common\Persistence\ObjectRepository;

interface UserRepositoryInterface extends ObjectRepository
{
    /**
     * @param  int $twitchUserId
     *
     * @return  User
     * 
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findByTwitchUserIdOrFail(int $twitchUserId): User;

    /**
     * @param  int $twitchUserId
     *
     * @return  null|User
     */
    public function findByTwitchUserId(int $twitchUserId): ?User;
}