<?php

declare(strict_types=1);

namespace App\Contracts\Repository;

use Doctrine\Common\Collections\Selectable;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * This interface signals that your repository should be loaded from the container.
 */
interface ServiceEntityRepositoryInterface extends ObjectRepository, Selectable
{
}
