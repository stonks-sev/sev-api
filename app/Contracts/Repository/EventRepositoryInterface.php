<?php

declare(strict_types=1);

namespace App\Contracts\Repository;

use Doctrine\Common\Persistence\ObjectRepository;

interface EventRepositoryInterface extends ObjectRepository
{
}