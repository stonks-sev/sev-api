<?php

declare(strict_types=1);

namespace App\Contracts\Repository;

use App\Entities\Streamer;
use Doctrine\Common\Persistence\ObjectRepository;

interface StreamerRepositoryInterface extends ObjectRepository
{
    /**
     * @param  int $streamerId
     *
     * @return  Streamer
     * @throws  NoResultException
     * @throws  NonUniqueResultException
     */
    public function findByStreamerIdOrFail(int $streamerId): Streamer;

    /**
     * @param  int $streamerId
     *
     * @return  null|Streamer
     */
    public function findByStreamerId(int $streamerId): ?Streamer;
}