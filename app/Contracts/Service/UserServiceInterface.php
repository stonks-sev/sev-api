<?php

declare(strict_types=1);

namespace App\Contracts\Service;

use App\Contracts\Http\Requests\UserLoginRequestInterface;
use App\Entities\Streamer;
use App\Entities\User;

/**
 * Interface UserServiceInterface
 */
interface UserServiceInterface
{
    /**
     * @param App\Contracts\Http\Requests\UserLoginRequestInterface $request
     * @param int                                                   $twitchUserId
     *
     * @return \App\Entities\User
     */
    public function create(UserLoginRequestInterface $request, int $twitchUserId): User;

    /**
     * @param  User     $user
     * @param  Streamer $streamer
     *
     * @return  \App\Entities\User
     */
    public function setFavouriteStreamer(User $user, Streamer $streamer): User;

    /**
     * @param  \App\Entities\User $user
     *
     * @return \App\Entities\User
     */
    public function removeFavouriteStreamer(User $user): User;
}
