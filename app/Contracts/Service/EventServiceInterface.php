<?php

declare(strict_types=1);

namespace App\Contracts\Service;

use App\Entities\Event;

/**
 * Interface EventServiceInterface
 */
interface EventServiceInterface
{
    /**
     * @param int $streamer
     * @param int $type
     * @param string $viewerName
     *
     * @return  \App\Entities\Event
     */
    public function create(int $streamerId, int $type, string $viewerName): Event;
}
