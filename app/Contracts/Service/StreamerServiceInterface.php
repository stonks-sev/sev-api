<?php

declare(strict_types=1);

namespace App\Contracts\Service;

use App\Contracts\Http\Requests\SetFavStreamerRequestInterface;
use App\Entities\Streamer;

/**
 * Interface StreamerServiceInterface
 */
interface StreamerServiceInterface
{
    /**
     * @param App\Contracts\Http\Requests\SetFavStreamerRequestInterface $request
     *
     * @return \App\Entities\Streamer
     */
    public function create(SetFavStreamerRequestInterface $request): Streamer;

    /**
     * @param  int $streamerId
     *
     * @return  \App\Entities\Streamer
     *
     * @throws    \Doctrine\ORM\NoResultException
     * @throws    \Doctrine\ORM\NonUniqueResultException
     */
    public function show(int $streamerId): Streamer;

    /**
     * @param  int $streamerId
     *
     * @return  bool
     *
     * @throws    \Doctrine\ORM\NoResultException
     * @throws    \Doctrine\ORM\NonUniqueResultException
     */
    public function delete(int $streamerId): bool;
}
